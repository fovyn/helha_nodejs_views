function ready(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else {
        document.addEventListener('DOMContentLoaded', fn);
    }
}

String.prototype.ucFirst = function() {
    const letter = this.substr(0,1);
    return `${letter.toUpperCase()}${this.substring(1)}`;
};
function changeForm() {
    const divs = document.querySelectorAll("div[form]");
    const form = document.querySelector("form");

    console.log(divs, form);
    if (divs.length <= 0) {
    } else {
        form.removeChild(this);
        for(let div of divs) {
            const formInput = div.getAttribute("form");
            if (formInput) {
                const divInputField = document.createElement("div");
                divInputField.classList.add("input-field");

                const input = document.createElement("input");
                input.name = formInput;
                input.value = div.innerText;

                const label = document.createElement("label");
                label.innerText = formInput.ucFirst();
                label.setAttribute("for", formInput);
                label.classList.add("active");
                divInputField.appendChild(input);
                divInputField.appendChild(label);
                form.appendChild(divInputField);
            }
            form.removeChild(div);
        }
        // this.setAttribute("type", "submit");
        const submitButton = document.createElement("button");
        submitButton.innerText = "Modifier";
        submitButton.classList.add("btn");
        form.appendChild(submitButton);
    }
}
ready(() => {
    const btn = document.querySelector("#edit");
    btn.addEventListener('click', changeForm);
});
