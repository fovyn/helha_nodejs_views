const express = require('express');
const {BookModule} = require('./modules/book/book.module');
const {UserModule} = require('./modules/user/user.module');
const path = require('path');
const bodyParser = require('body-parser');
const db = require('./models');
const cors = require('cors');

const app = express();

/**
 * Définition du moteur de template avec son répertoire
 */
app.set("views", path.join(__dirname, 'views'));
app.set("view engine", "pug");

/**
 * Déclaration des différents middleware
 */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(cors());

for(let item of BookModule.Routers) {
    setTimeout(() => app.use(item.url, item.router), 20);
}

for(let item of UserModule.Routers) {
    console.log(item);
    setTimeout(() => app.use(item.url, item.router), 20);
}

db.sequelize.sync({force: true}).then(async () => {
    await db.user.create({username: "Flavian", password: "Flavian"});
    await db.book.create({title: "Test", description: "Test", uploaderId: 1});
    // const role = await db.role.create({label: "ROLE_USER"});
    // const user = await db.user.create({username: "Flavian", password: "Flavian"});
    // user.addRoles(role);
    //
    // await db.user.update(user);
    /**
     * Lancement du server sur le port 3000
     */
    app.listen(3000, () => console.log(`Server listening on url http://localhost:3000`));
});
