const {UserController} = require('./controllers/user.controller');
const {UserRouter} = require('./routes/user.route');

exports.UserModule = {
    Controllers: [UserController],
    Routers: [{url: '/user', router: UserRouter}],
    Services: []
};
