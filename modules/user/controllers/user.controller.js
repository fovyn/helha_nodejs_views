const path = require('path');
const db = require("../../../models");
const UserController = function() {
    /**
     * Permet l'ajout en base de donnée d'un nouvel utilisateur
     * @param body contient toutes les données du nouvel utilisateur
     * @param res utilisé pour renvoyé la réponse
     * @returns {Promise<void>} le type de la réponse
     */
    this.createAction = async ({body}, res) => {
        const userCreate = await db.user.create(body);

        res.json(userCreate);
    };
    /**
     * Permet la lecture de tous les utilisateurs
     * @param req rien de particulier dans ce cas
     * @param res utilisé pour créer la réponse a renvoyé
     * @returns {Promise<void>}
     */
    this.readAllAction = async (req, res) => {
        res.json(await db.user.findAll());
    };
    /**
     * Permet la lecture d'un utilisateur selon son identifiant
     * @param id
     * @param res
     * @returns {Promise<void>}
     */
    this.readOneById = async ({params: {id}}, res) => {
        res.json(await db.user.findByPk(id));
    };
    /**
     * Permet la lecture d'un utilisateur selon son username
     * @param username
     * @param res
     * @returns {Promise<void>}
     */
    this.readOneByUsername = async ({params: {username}}, res) => {
        res.json(await db.user.findOne({where: {username}}));
    };
    /**
     *
     * @param id
     * @param body
     * @param res
     * @returns {Promise<void>}
     */
    this.partialUpdatection = async ({params: {id}, body}, res) => {
        res.json(await db.user.update(body, {where: {id}}));
    };

    /**
     * Permet la suppresion d'un utilisateur dans la base de donnée !!!normalement, une suppression logique + sécurité!!!!
     * @param id
     * @param res
     * @returns {Promise<void>}
     */
    this.deletAction = async ({params: {id}}, res) => {
        await db.user.destroy({where: {id}});
        res.json()
    };

    /**
     * Permet la lecture de tous les livres ajoutés par l'utilisateur
     * @param id
     * @param res
     * @returns {Promise<void>}
     */
    this.readBookByUserId = async ({params: {id}, res}) => {
        res.json(await db.book.findAll({where: {uploaderId: id}}));
    }
};

const userController = new UserController();
exports.UserController = userController;
