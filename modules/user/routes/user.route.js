const express = require('express');
const {UserController} = require('../controllers/user.controller');
const router = express.Router();

router.get(["", "/"], UserController.readAllAction);
router.get(["/:id([0-9]+)"], UserController.readOneById);
router.get(["/:id([0-9]+)/book"], UserController.readBookByUserId);
// router.get(["/:id(\d+)/book"], UserController.readBookByUserId);
router.get(["/:username([a-zA-Z]+)"], UserController.readOneByUsername);
router.post(["", "/"], UserController.createAction);
router.patch(["/:id([0-9]+)"], UserController.partialUpdatection);
router.delete(["/:id([0-9]+)"], UserController.deletAction);

exports.UserRouter = router;
