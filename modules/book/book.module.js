const {BookController} = require('./controllers/book.controller');
const {BookRouter} = require('./routes/book.route');

exports.BookModule = {
    Controllers: [BookController],
    Routers: [{url: '/book', router: BookRouter}],
    Services: []
};
