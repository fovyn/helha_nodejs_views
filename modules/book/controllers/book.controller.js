const BookController = (function() {
    this.books = [];
    this.nbItem = 0;
    /**
     * Action permettant l'affichage de l'ensemble des livres
     * @param req
     * @param res
     */
    this.listAction = (req, res) => {
        res.render('book/list', {books: this.books});
    };
    /**
     * Action permettant l'affichage des détails d'un livre
     * @param id
     * @param res
     */
    this.showAction = ({params: {id}}, res) => {
        const idParsed = parseInt(id);
        res.render('book/show', {book: this.books.find(b => b.id === idParsed)});
    };
    /**
     * Action permettant l'édition d'un livre
     * @param id Paramètre de la route
     * @param body Contenu du formulaire d'édition d'un livre
     * @param res Réponse a renvoyé
     */
    this.editAction = ({params: {id}, body}, res) => {
        const idParsed = parseInt(id);
        //Recherche du livre à éditer
        const bookToEdit = this.books.find(b => b.id === idParsed);
        //Assigner les modification du formulaire à la référence du livre à éditer
        Object.assign(bookToEdit, {title: body.title, description: body.description});
        //Rediriger vers l'afficher de le liste
        res.redirect(`/book/list`);
    };

    /**
     * Action permettant la suppression d'un livre
     * @param id
     * @param res
     */
    this.deleteAction = ({params: {id}}, res) => {
        const bookToDeleteIndex = this.books.findIndex(b => b.id === id);

        //Suppprime la cellule du tableau à l'indice de départ et le deuxième paramètre c'est le nombre d'élément a supprimer
        this.books.splice(bookToDeleteIndex, 1);
        res.end();
    };
    /**
     * Action permettant l'affichage du formulaire de création
     * @param req
     * @param res
     */
    this.createAction = (req, res) => {
        res.render('book/create');
    };
    /**
     * Action permettan la gestion de la soumission du formulaire de création
     * @param body
     * @param res
     */
    this.createPostAction = ({body}, res) => {
        const book = {id: ++this.nbItem};
        Object.assign(book, body);
        this.books.push(book);
        res.redirect('/book/list');
    };
});

exports.BookController = new BookController();
