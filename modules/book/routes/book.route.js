const express = require('express');
const {BookController} = require('../controllers/book.controller');

const router = express.Router();

//get|post|patch|put|delete|option => Methode HTTP Utilisé
//Prmeier paramètre permet la recherche de l'url selon les possibilite
//Deuxième paramètre permet de définir l'action à éxecuter
router.get(['', '/list', '/'], BookController.listAction);
router.get(['/:id([0-9]+)'], BookController.showAction);
router.post(['/:id([0-9]+)'], BookController.editAction);
router.post(['', '/'], BookController.createPostAction);
// router.put(['/:id(\d+)'],);
router.get(['/create'], BookController.createAction);
router.delete(['/:id'], BookController.deleteAction);

exports.BookRouter = router;
