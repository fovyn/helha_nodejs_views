module.exports = (sequelize, DataTypes) => {
    const BookCase = sequelize.define("bookCase", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true
        }
    }, { freezeTableName: true });
    BookCase.associate = (models) => {
        // BookCase.belongsTo(models.user, {as: "creator", foreignKey: "FK_BookCase_User"});
    };

    return BookCase;
};
