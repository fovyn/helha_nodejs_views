module.exports = (sequelize, DataTypes) => {
    const Author = sequelize.define("author", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true
        }
    }, { freezeTableName: true });
    Author.associate = (models) => {
        // Author.hasMany(models.book, {as: "books"});
    };

    return Author;
};
