module.exports = (sequelize, DataTypes) => {
    const Book = sequelize.define("book", {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        titre: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true
        },
        description: {
            type: DataTypes.STRING
        }
    }, { freezeTableName: true });
    Book.associate = (models) => {
        Book.belongsTo(models.author, {as: "author"});
        Book.belongsTo(models.user, {as: "uploader"});
    };

    return Book;
};
