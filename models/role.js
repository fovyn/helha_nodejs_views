// const { DataTypes } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define("role", {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true
        },
        label: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true,
        }
    }, {
        freezeTableName: true
    });

    return Role;
};
