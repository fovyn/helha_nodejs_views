// const { DataType } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("user", {
        id: {
            type: DataTypes.BIGINT,
            autoIncrement: true,
            primaryKey: true,
        },
        username: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true
        },
        password: {
            type: DataTypes.STRING,
            notNull: true
        }
    }, {
        freezeTableName: true
    });

    User.associate = (models) => {
        User.belongsToMany(models.role, {through: 'User_Roles', as: 'roles', foreignKey: 'user_id'});
        // User.hasMany(models.book, {as: "books"});
        User.hasOne(models.bookCase, { as: "bookCase", foreignKey: "user_id"});
    };

    return User;
};
